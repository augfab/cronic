#!/usr/bin/env bash

# Cronic v3 - cron job report wrapper
# Copyright 2007-2016 Chuck Houpt. No rights reserved, whatsoever.
# Public Domain CC0: http://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail

TMP="$(mktemp -d)"
trap 'rm -rf "${TMP}"' EXIT

main () {
    local out err trace result pattern
    out="${TMP}"/cronic.out
    err="${TMP}"/cronic.err
    trace="${TMP}"/cronic.trace

    set +e
    "$@" >"${out}" 2>"${trace}"
    result="$?"
    set -e

    pattern="^${PS4:0:1}\\+${PS4:1}"
    if grep -aq "${pattern}" "${trace}"; then
        ! grep -av "${pattern}" "${trace}" > "${err}"
    else
        err="${trace}"
    fi

    if (( result != 0 )) || [[ -s "${err}" ]]; then
        printf 'Cronic detected failure or error output for the command:\n'
        printf '%s\n' "$*"
        printf '\nRESULT CODE: %d\n' "${result}"
        if [[ -s "${err}" ]]; then
            printf '\nERROR OUTPUT:\n'
            cat "${err}"
        fi
        printf '\nSTANDARD OUTPUT:\n'
        cat "${out}"
        if [[ "${trace}" != "${err}" ]]; then
            printf '\n'
            printf 'trace-ERROR OUTPUT:\n'
            cat "${trace}"
        fi
    fi
}

main "$@"
